const createError  = require('http-errors');
const express      = require('express');
const path         = require('path');
const cookieParser = require('cookie-parser');
const logger       = require('morgan');
const cors         = require('cors');

const indexRouter           = require('./routes/index');
const entriesRouter         = require('./routes/entries');
const questionsRouter       = require('./routes/questions');
const adminRouter           = require('./routes/admin');
const recommendationsRouter = require('./routes/recommendations');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/', indexRouter);
app.use('/questions', questionsRouter);
app.use('/entries', entriesRouter);
app.use('/recommendations', recommendationsRouter);
app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(( req, res, next ) => next(createError(404)));

// error handler
app.use(function ( err, req, res, next ) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error   = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
