const express    = require('express');
const router     = express.Router();
const controller = require('../controllers/recommendations');

router.param('id', controller.load);
router.get('/', controller.list);
router.get('/:id', controller.single);

module.exports = router;
