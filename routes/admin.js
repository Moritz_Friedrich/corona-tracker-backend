const express    = require('express');
const router     = express.Router();
const controller = require('../controllers/admin');

router.get('/', controller.dashboard);

router.get('/system/stats', controller.stats);

router.get('/entries', controller.entries);

router.param('question', controller.loadQuestion);
router.get('/questions', controller.questions);
router.get('/questions/add', ( req, res ) => res.redirect('/admin/questions'));
router.post('/questions/add', controller.createQuestion);
router.post('/questions/:question/delete', controller.deleteQuestion);
router.get('/questions/:question/edit', controller.editQuestion);
router.post('/questions/:question/edit', controller.updateQuestion);

router.get('/recommendations', controller.recommendations);
router.post('/recommendations/add', controller.createRecommendation);

module.exports = router;
