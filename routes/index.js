const express = require('express');
const router  = express.Router();

const mongoose = require('mongoose');
const Entry    = mongoose.model('Entry');

router.get('/', async ( req, res ) => {
  const entryCount = await Entry.countDocuments();

  return res.render('index', {
    title: 'Übersicht',
    entryCount,
  });
});

module.exports = router;
