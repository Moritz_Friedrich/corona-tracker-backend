const express    = require('express');
const router     = express.Router();
const controller = require('../controllers/entries');

router.param('id', controller.load);
router.get('/', controller.list);
router.post('/', controller.create);
router.get('/:id', controller.single);
router.get('/:id/score', controller.score);

module.exports = router;
