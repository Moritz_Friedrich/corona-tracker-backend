document.addEventListener('DOMContentLoaded', () => {
  document.querySelector('#add-question').addEventListener('click', () => window.location = '/admin/questions');
  document.querySelector('#add-recommendation').addEventListener('click', () => window.location = '/admin/recommendations');
});
