document.addEventListener('DOMContentLoaded', () => {
  const backButton = document.querySelector('#back');

  if ( backButton ) {
    backButton.addEventListener('click', () => window.location.href = '/admin');
  }
});
