const mongoose = require('mongoose');
const fs       = require('fs');
const { join } = require('path');

const models = join(__dirname, 'models');

module.exports = async function connect ( uri ) {
  // Load all models
  fs.readdirSync(models)
    .filter(file => ~file.search(/^[^.].*\.js$/))
    .forEach(file => require(join(models, file)));

  // Add the error handler, reconnect on disconnects
  mongoose.connection
    .on('error', console.error)
    .on('disconnected', connect);

  // Connect to the database server
  return mongoose.connect(uri, {
    keepAlive:          1,
    useNewUrlParser:    true,
    useUnifiedTopology: true,
  });
};

