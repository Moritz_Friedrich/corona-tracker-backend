const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const RecommendationSchema = new Schema({
  text:     { type: String, default: '' },
  minScore: { type: Number },
  maxScore: { type: Number },
});

RecommendationSchema.methods = {};

RecommendationSchema.statics = {
  load ( _id ) {
    return this.findOne({ _id });
  },

  findByScore ( score ) {
    return this.findOne({
      $and: [
        { minScore: { $lt: score } },
        { maxScore: { $gte: score } },
      ],
    });
  },

  list ( options = {} ) {
    const criteria = options.criteria || {};
    const page     = options.page || 0;
    const limit    = options.limit || 30;
    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .exec();
  },
};

mongoose.model('Recommendation', RecommendationSchema);
