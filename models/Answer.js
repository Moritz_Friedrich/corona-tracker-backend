const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const AnswerSchema = new Schema({
  text:      { type: String },
  date:      { type: Date },
  choices:   [ { type: Schema.ObjectId, ref: 'Option' } ],
  question:  { type: Schema.ObjectId, ref: 'Question' },
  createdAt: { type: Date, default: Date.now },
});

AnswerSchema.methods = {};

AnswerSchema.statics = {
  load ( _id ) {
    return this.findOne({ _id });
  },

  list ( options ) {
    const criteria = options.criteria || {};
    const page     = options.page || 0;
    const limit    = options.limit || 30;
    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .exec();
  },
};

mongoose.model('Answer', AnswerSchema);
