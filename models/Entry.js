const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const EntrySchema = new Schema({
  deviceId:                   { type: String },
  deviceType:                 { type: String, enum: [ 'IOS', 'ANDROID' ] },
  age:                        { type: Number },
  gender:                     { type: String },
  numberFamilyMembersInHouse: { type: Number },
  zipCode:                    { type: String },

  // See https://mongoosejs.com/docs/geojson.html
  location:  {
    type:        { type: String, enum: [ 'Point' ], required: true },
    coordinates: { type: [ Number ], required: true },
  },
  createdAt: { type: Date, default: Date.now },
  answers:   [ { type: Schema.ObjectId, ref: 'Answer' } ],
});

EntrySchema.methods = {
  async calculateScore () {

    // Start with a neutral 0 score
    let score = 0;

    // Iterate all answers in this survey entry
    for ( const answer of this.answers ) {
      score = answer.choices

        // Multiply the question weight with the choice multiplier
        .map(choice => choice.multiplier * answer.question.weight)

        // Sum up all the points and add them to the score
        .reduce(( sum, points ) => sum + points, score);
    }

    return score;
  },
};

EntrySchema.statics = {
  load ( _id ) {
    return this
      .findOne({ _id }, {}, { sort: { datetime: -1 } })
      .populate({
        path:     'answers',
        populate: {
          path: 'question choices',
        },
      })
      .exec();
  },

  findByDeviceId ( deviceId ) {
    return this
      .findOne({ deviceId }, {}, { sort: { datetime: -1 } })
      .populate({
        path:     'answers',
        populate: {
          path: 'question choices',
        },
      })
      .exec();
  },

  findByIdOrDeviceId ( id ) {
    return this
      .findOne({
        $or: [
          { _id: id },
          { deviceId: id },
        ],
      }, {}, { sort: { datetime: -1 } })
      .populate({
        path:     'answers',
        populate: {
          path: 'question choices',
        },
      })
      .exec();
  },

  list ( options ) {
    const criteria = options.criteria || {};
    const page     = options.page || 0;
    const limit    = options.limit || 30;
    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .populate('answers')
      .exec();
  },
};

mongoose.model('Entry', EntrySchema);
