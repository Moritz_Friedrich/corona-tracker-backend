const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const OptionSchema = new Schema({
  text:       { type: String, default: '', maxlength: 4096 },
  multiplier: { type: Number, default: 0 },
  createdAt:  { type: Date, default: Date.now },
});

OptionSchema.methods = {};

OptionSchema.statics = {
  load ( _id ) {
    return this.findOne({ _id });
  },

  list ( options ) {
    const criteria = options.criteria || {};
    const page     = options.page || 0;
    const limit    = options.limit || 30;
    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .exec();
  },
};

mongoose.model('Option', OptionSchema);
