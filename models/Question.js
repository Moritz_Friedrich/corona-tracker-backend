'use strict';

const mongoose = require('mongoose');
const Schema   = mongoose.Schema;

const getTags = tags => tags.join(',');
const setTags = tags => {
  if ( !Array.isArray(tags) ) {
    return tags.split(',').slice(0, 10); // max tags
  }

  return [];
};

const QuestionSchema = new Schema({
  title:  { type: String, default: '' },
  text:   { type: String, default: '', maxlength: 512 },
  weight: { type: Number, default: 1 },

  answerType: {
    type:    String,
    default: 'FREETEXT',
    enum:    [ 'MULTICHOICE', 'SINGLECHOICE', 'FREETEXT' ],
  },

  required: {
    type:    Boolean,
    default: true,
  },

  possibleAnswers: [ { type: Schema.ObjectId, ref: 'Option' } ],

  tags: { type: [], get: getTags, set: setTags },

  createdAt: { type: Date, default: Date.now },
});

/**
 * Pre-remove hook
 QuestionSchema.pre('remove', next => {
  next();
});
 */

QuestionSchema.methods = {};

QuestionSchema.statics = {
  load ( _id ) {
    return this.findOne({ _id })
      .populate('possibleAnswers', '_id text')
      .exec();
  },

  findById ( id ) {
    return this.findOne({ _id: id })
      .populate('possibleAnswers')
      .exec();
  },

  list ( options = {} ) {
    const criteria = options.criteria || {};
    const page     = options.page || 0;
    const limit    = options.limit || 30;

    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .populate('possibleAnswers', '_id text')
      .exec();
  },
};

mongoose.model('Question', QuestionSchema);
