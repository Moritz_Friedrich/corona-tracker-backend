const mongoose       = require('mongoose');
const Recommendation = mongoose.model('Recommendation');

exports.load = async function ( request, response, next, id ) {
  const recommendation = Recommendation.load(id);

  if ( !recommendation ) {
    return response
      .status(404)
      .send({ message: 'Recommendation not found' });
  }

  request.recommendation = recommendation;

  return next();
};

exports.list = async function ( request, response ) {
  const page    = ( request.query.page > 0 ? request.query.page : 1 ) - 1;
  const _id     = request.query.item;
  const limit   = Math.min(Math.max(1, request.query.per_page || 30), 30);
  const options = { limit, page };

  if ( _id ) {
    options.criteria = { _id };
  }

  const entries = await Recommendation.list(options);
  const count   = await Recommendation.countDocuments();

  return response.send({
    items:    entries,
    page:     page + 1,
    per_page: limit,
    pages:    Math.ceil(count / limit),
  });
};

exports.single = async function ( request, response ) {
  return response.send(request.entry);
};
