const mongoose = require('mongoose');
const Question = mongoose.model('Question');

exports.load = async function ( request, response, next, id ) {
  const question = await Question.load(id);

  if ( !question ) {
    return response.send({ message: 'Question not found' }, 404);
  }

  request.question = question;

  return next();
};

exports.list = async function ( request, response ) {
  const page    = ( request.query.page > 0 ? request.query.page : 1 ) - 1;
  const _id     = request.query.item;
  const limit   = Math.min(Math.max(1, request.query.per_page || 30), 30);
  const options = { limit, page };

  if ( _id ) {
    options.criteria = { _id };
  }

  const questions = await Question.list(options);
  const count     = await Question.countDocuments();

  return response.send({
    items:    questions,
    page:     page + 1,
    per_page: limit,
    pages:    Math.ceil(count / limit),
  });
};

exports.single = async function ( request, response ) {
  return response.send(request.question);
};
