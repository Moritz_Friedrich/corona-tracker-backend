const os             = require('os');
const pidUsage       = require('pidusage');
const prettyBytes    = require('pretty-bytes');
const mongoose       = require('mongoose');
const Question       = mongoose.model('Question');
const Option         = mongoose.model('Option');
const Entry          = mongoose.model('Entry');
const Recommendation = mongoose.model('Recommendation');

async function fetchStats () {
  const [ database, system ] = await Promise.all([
    mongoose.connection.db.stats(),
    pidUsage(process.pid),
  ]);

  const totalMemory = os.totalmem();
  const freeMemory  = os.freemem();
  const usedMemory  = totalMemory - freeMemory;

  return {
    database: {
      ...database,
      prettyAverageObjectSize: prettyBytes(database.avgObjSize),
      prettyIndexSize:         prettyBytes(database.indexSize),
      usedStorage:             prettyBytes(database.fsUsedSize),
      usedStoragePercentage:   ( database.fsUsedSize / database.fsTotalSize ) * 100,
      availableStorage:        prettyBytes(database.fsTotalSize),
    },
    system:   {
      ...system,
      totalMemory,
      freeMemory,
      usedMemory:        totalMemory - freeMemory,
      prettyUsedMemory:  prettyBytes(usedMemory),
      prettyTotalMemory: prettyBytes(totalMemory),
      commandLine:       process.argv.join(' '),
      title:             process.title,
    },
  };
}

exports.loadQuestion = async function ( request, response, next, id ) {
  const question = await Question.findById(id);

  if ( !question ) {
    return response.send({ message: 'Question not found' }, 404);
  }

  request.question = question;

  return next();
};

exports.dashboard = async function ( request, response ) {
  const stats                                              = await fetchStats();
  const [ entryCount, questionCount, recommendationCount ] = await Promise.all([
    Entry.countDocuments(),
    Question.countDocuments(),
    Recommendation.countDocuments(),
  ]);

  return response.render('admin/dashboard', {
    title: 'Dashboard',
    entryCount,
    questionCount,
    recommendationCount,
    stats,
  });
};

exports.stats = async function ( request, response ) {
  return response.send(await fetchStats());
};

exports.entries = async function ( request, response ) {
  const page    = ( request.query.page > 0 ? request.query.page : 1 ) - 1;
  const _id     = request.query.item;
  const limit   = Math.min(Math.max(1, request.query.per_page || 30), 30);
  const options = { limit, page };

  if ( _id ) {
    options.criteria = { _id };
  }

  const entries = await Entry.list(options);
  const count   = await Entry.countDocuments();

  return response.render('admin/entries', {
    title:    'Entries',
    entries,
    page:     page + 1,
    per_page: limit,
    pages:    Math.ceil(count / limit),
  });
};

exports.questions = async function ( request, response ) {
  const questions = await Question.list();

  return response.render('admin/add_question', {
    title: 'Add a question',
    questions,
  });
};

exports.createQuestion = async function ( request, response ) {
  const { body } = request;
  const question = new Question(body);

  if ( body.possibleAnswers && Array.isArray(body.possibleAnswers) ) {
    question.possibleAnswers = await Promise.all(
      body.possibleAnswers

        // Remove any empty answers
        .filter(text => !!text)

        // Save all options
        .map(text => {
          const index = body.possibleAnswers.indexOf(text);

          // We know there's one multiplier input per answer input, so the index
          // of our answer input must be the same as its multiplier index
          const multiplier   = body.multipliers[ index ] || 0;
          const choiceOption = new Option({ text, multiplier });

          return choiceOption.save();
        }),
    );

    if ( body.answerType === 'FREETEXT' ) {
      question.answerType = 'SINGLECHOICE';
    }
  }

  await question.save();

  return response.redirect('/admin/questions');
};

exports.deleteQuestion = async function ( request, response ) {
  await Question.remove({ _id: request.question._id });

  return response.redirect('/admin/questions');
};

exports.editQuestion = async function ( request, response ) {
  return response.render('admin/edit_question', {
    title:    `Edit question ${ request.question._id }`,
    question: request.question,
  });
};

exports.updateQuestion = async function ( request, response ) {
  const { body } = request;
  const question = request.question;

  question.title      = body.title;
  question.text       = body.text;
  question.weight     = body.weight;
  question.answerType = body.answerType;

  if ( body.possibleAnswers && Array.isArray(body.possibleAnswers) ) {
    question.possibleAnswers = await Promise.all(
      body.possibleAnswers

        // Remove any empty answers
        .filter(text => !!text)

        // Save all options
        .map(text => {
          const index = body.possibleAnswers.indexOf(text);

          // We know there's one multiplier input per answer input, so the index
          // of our answer input must be the same as its multiplier index
          const multiplier   = body.multipliers[ index ] || 0;
          const choiceOption = new Option({ text, multiplier });

          return choiceOption.save();
        }),
    );
  }

  await question.save();

  return response.redirect('/admin/questions');
};

exports.recommendations = async function ( request, response ) {
  const recommendations = await Recommendation.list();

  return response.render('admin/recommendations', {
    title: 'Recommendations',
    recommendations,
  });
};

exports.createRecommendation = async function ( request, response ) {
  const recommendation = new Recommendation(request.body);

  await recommendation.save();

  return response.redirect('/admin/recommendations');
};
