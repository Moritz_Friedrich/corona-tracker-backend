const mongoose       = require('mongoose');
const Entry          = mongoose.model('Entry');
const Answer         = mongoose.model('Answer');
const Recommendation = mongoose.model('Recommendation');
const { isValid }    = require('mongodb').ObjectID;

/**
 * Checks whether a string is a date string
 *
 * @param {string} string
 * @return {boolean}
 */
function isDateString ( string ) {
  const isoPattern = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;

  return isoPattern.test(string);
}

exports.load = async function ( request, response, next, id ) {
  const entry = isValid(id)
    ? await Entry.findByIdOrDeviceId(id)
    : await Entry.findByDeviceId(id);

  if ( !entry ) {
    return response
      .status(404)
      .send({ message: 'Entry not found' });
  }

  request.entry = entry;

  return next();
};

exports.list = async function ( request, response ) {
  const page    = ( request.query.page > 0 ? request.query.page : 1 ) - 1;
  const _id     = request.query.item;
  const limit   = Math.min(Math.max(1, request.query.per_page || 30), 30);
  const options = { limit, page };

  if ( _id ) {
    options.criteria = { _id };
  }

  const entries = await Entry.list(options);
  const count   = await Entry.countDocuments();

  return response.send({
    items:    entries,
    page:     page + 1,
    per_page: limit,
    pages:    Math.ceil(count / limit),
  });
};

exports.single = async function ( request, response ) {
  return response.send(request.entry);
};

exports.score = async function ( request, response ) {
  const score    = await request.entry.calculateScore();
  const { text } = await Recommendation.findByScore(score);

  return response.send({
    recommendation: text,
    score,
  });
};

exports.create = async function ( request, response ) {
  const { body } = request;

  // Create an entry from the input data, mapping the fields
  const entry = new Entry({
    deviceId:                   body.DeviceID,
    deviceType:                 body.DeviceType,
    age:                        body.Age,
    gender:                     body.Gender,
    numberFamilyMembersInHouse: body.NumberFamilyMembersInHouse,
    createdAt:                  body.SubmitDatetime,
    zipCode:                    body.PLZ,
    location:                   {
      type:        'Point',
      coordinates: [
        body.Location.LONG,
        body.Location.LAT,
      ],
    },
  });

  // Parse all answers in the input data, resolve them to options if possible
  entry.answers = await Promise.all(
    ( body.Survey || body.survey )

      // Filter out any invalid question IDs
      .filter(( { questionid } ) => isValid(questionid))

      // Map the answers to actual answer entities
      .map(async ( { questionid: questionId, answer: choice } ) => {
        const answer = new Answer({
          question: questionId,
        });

        // If the answer is an array or a valid object ID, they are one or more
        // pre-written answer options. Otherwise, it's a free-text answer or
        // date string.
        if ( !Array.isArray(choice) && !isValid(choice) ) {
          if ( isDateString(choice) ) {
            answer.date = new Date(choice);
          } else {
            answer.text = choice;
          }
        } else {
          const choices  = Array.isArray(choice) ? choice : [ choice ];
          answer.choices = choices.filter(item => isValid(item));
        }

        // Save the answer. This will return a promise for the answer entity.
        return answer.save();
      }),
  );

  // Save the entry
  await entry.save();

  return response.status(202).end();
};
